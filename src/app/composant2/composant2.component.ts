import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-composant2',
  templateUrl: './composant2.component.html',
  styleUrls: ['./composant2.component.css']
})
export class Composant2Component implements OnInit {

  poney: object;
  selectTrois: boolean;
  foreground: string;

  constructor() { }

  ngOnInit() {
    this.poney = {
      name: 'Pinkie Pie'
    }
    this.selectTrois = true;
    this.foreground = 'red';
  }

  onButtonClick(): void {
    alert('coucou');
  }

}

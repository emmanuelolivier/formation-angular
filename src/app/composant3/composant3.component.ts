import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-composant3',
  templateUrl: './composant3.component.html',
  styleUrls: ['./composant3.component.css']
})
export class Composant3Component implements OnInit {

  arriveeCourse: Array<string>;
  poneys: Array<string>;

  constructor() { }

  ngOnInit() {
    this.arriveeCourse = [];

    this.poneys = [
      'Twilight Sparkle',
      'Spike',
      'Applejack',
      'Fluttershy',
      'Rarity',
      'Pinkie Pie',
      'Rainbow Dash',
      'Princesse Celestia',
      'Princesse Luna',
      'Princesse Cadance']
  }

  addToArriveeCourse() {
    const index = Math.floor(Math.random() * this.poneys.length);
    const unPoneyAuHasard = this.poneys[index];
    this.poneys.splice(index, 1);
    this.arriveeCourse.push(unPoneyAuHasard);
    console.log(this.arriveeCourse);
  }

  lancePremiereAlerte(event): void {
    alert('div 1');
  }

  lanceSecondeAlerte(event): void {
    event.stopPropagation();
    alert('div 2');
  }

  onAltVPress(): void {
    console.log('On a cliqué sur Alt et V !');
  }

  afficheValue(value: string): void {
    alert(value);
  }
}
